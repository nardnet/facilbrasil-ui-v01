import { Component, OnInit } from '@angular/core';
import { ToastyConfig } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../seguranca/auth.service';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Clientes } from '../../core/model';
import { ClienteService } from '../../clientes/cliente.service';

@Component({
  selector: 'app-titulos-principal',
  templateUrl: './titulos-principal.component.html',
  styleUrls: ['./titulos-principal.component.css']
})
export class TitulosPrincipalComponent implements OnInit {
  cliente = new Clientes();
  saldoACobrar: number;
  saldoCobrado: number;
  constructor(
    private clienteService: ClienteService,
    private toastyConfig: ToastyConfig,
    private router: Router,
    public auth: AuthService,
    private errorHandler: ErrorHandlerService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    const codAuth = this.auth.jwtPayload.codigoCliente;
    const codigoCliente = this.route.snapshot.params['codigoCliente'];
    this.saldoACobrar = this.auth.jwtPayload.saldoACobrar;
    this.saldoCobrado = this.auth.jwtPayload.saldoCobrado;

    if (codAuth) {
      this.carregarCliente(codAuth);
    } else if (codigoCliente) {
      this.carregarCliente(codigoCliente);
    }
  }

  carregarCliente(codigo: number) {
    this.clienteService.buscarPorCodigo(codigo)
      .then(cliente => {
        this.cliente = cliente;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
