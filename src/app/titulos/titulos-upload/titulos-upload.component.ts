import { Component, OnInit, AfterContentChecked, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ToastyConfig, ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../seguranca/auth.service';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { TitulosService } from './../titulos.service';

@Component({
  selector: 'app-titulos-upload',
  templateUrl: './titulos-upload.component.html',
  styleUrls: ['./titulos-upload.component.css']
})
export class TitulosUploadComponent implements OnInit, AfterContentChecked {
  loading = false;
  codigoCliente: string;
  lancamentosUrl: string;
  @ViewChild('fileInput') fileInput: ElementRef;
  currentFileUpload: File;
  selectedFiles: FileList;
  displayDialog: boolean;
  saldoACobrar: string;
  quantidade: string;
  constructor(
    private titulosService: TitulosService,
    private toasty: ToastyService,
    public auth: AuthService,
    private errorHandler: ErrorHandlerService,
    private cdref: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.codigoCliente =  this.auth.jwtPayload.codigoCliente;
  }

  showDialogToAccept() {
    this.displayDialog = false;
    this.fileInput.nativeElement.click();
  }

  showDialogToReject() {
    this.quantidade = '0';
    this.saldoACobrar = '0,00';
     this.displayDialog = false;
  }


  setCurrentUpload(e) {
    this.currentFileUpload = e.item(0);
    if (this.currentFileUpload.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
        this.currentFileUpload.type !== 'application/vnd.ms-excel') {
      this.loading = false;
      return this.toasty.error(`É possivel importar somente arquivos .xlsx ou .xls`);
    }

    this.loading = true;
    this.titulosService.upload(this.codigoCliente, this.quantidade, this.saldoACobrar, this.currentFileUpload).then(relatorio => {

      this.toasty.success(`Importação concluida com sucesso!`);
      this.quantidade = '0';
      this.saldoACobrar = '0,00';
    }).catch(erro => this.errorHandler.handle(erro)).then(() => {
      this.loading = false;
    });
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  antesUploadAnexo(event) {
    this.loading = false;
    event.xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
  }

  openFileUpload() {
    this.quantidade = '0';
    this.saldoACobrar = '0,00';
    this.displayDialog = true;
  }

  download() {
    this.loading = true;
    return this.titulosService.download()
      .then(relatorio => {
        const url = window.URL.createObjectURL(relatorio);
        const anchor = document.createElement('a');
        anchor.download = 'Modelo_Importação.xlsx';
        anchor.href = url;
        anchor.click();
        this.loading = false;
       })
      .catch(erro => this.errorHandler.handle(erro)).then(() => {
        this.loading = false;
      });
  }

}
