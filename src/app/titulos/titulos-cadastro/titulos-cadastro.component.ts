import { TipoDividaService } from './../../tipo-divida/tipo-divida.service';
import { TipoDivida, Estado, Banco, Titulo, Clientes } from './../../core/model';
import { FormControl  } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { ClienteService } from '../../clientes/cliente.service';
import { ToastyService } from 'ng2-toasty';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Title } from '@angular/platform-browser';
import {ConfirmationService} from 'primeng/api';
import { BancosService } from '../../bancos/bancos.service';
import { EstadosService } from '../../estados/estados.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../seguranca/auth.service';


@Component({
  selector: 'app-titulos-cadastro',
  templateUrl: './titulos-cadastro.component.html',
  styleUrls: ['./titulos-cadastro.component.css']
})
export class TitulosCadastroComponent implements OnInit, AfterContentChecked {
  @ViewChild('salvaTitulo') salTitulo: ElementRef;
  @ViewChild('telefone2') tel2: ElementRef;
  @ViewChild('telefone1') tel1: ElementRef;
  @ViewChild('celular') cel: ElementRef;
  @ViewChild('cep') cepField: ElementRef;

  cliente = new Clientes();
  tipoDividas = new TipoDivida();
  estados = new Estado();
  bancos = new Banco();
  exbindoFormularioTitulo = false;
  cheque: boolean;
  titulo;
  dtVencimento;
  tituloIndex: number;
  temTituloIndex: number;
  exibirMaterTituto: boolean;
  pt = {};
  displayDialog: boolean;
  loading = false;

  constructor(
    private clienteService: ClienteService,
    private tipoDividaService: TipoDividaService,
    private bancosService: BancosService,
    private estadosService: EstadosService,
    private confirmationService: ConfirmationService,
    private toasty: ToastyService,
    private errorHandler: ErrorHandlerService,
    private title: Title,
    private route: ActivatedRoute,
    private auth: AuthService,
    private cdref: ChangeDetectorRef

  ) { }

  ngAfterContentChecked() {
    this.cdref.detectChanges();

  }

  ngOnInit() {
    const codAuth = this.auth.jwtPayload.codigoCliente;
    const codigoCliente = this.route.snapshot.params['codigoCliente'];
    this.titulo = new Titulo();
    this.titulo.banco = new Banco();
    this.carregarTipoDivida();
    this.carregarBancos();
    this.carregarEstados();
    this.title.setTitle('Cadastro de Títulos');


    if (codAuth) {
      this.carregarCliente(codAuth);
    } else if (codigoCliente) {
      this.carregarCliente(codigoCliente);
    }

    this.pt = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
      dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
        'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      today: 'Hoje',
      clear: 'Limpar'
    };

  }

  prepararNovoTitulo() {
    this.titulo = new Titulo();
    this.tituloIndex = this.cliente.titulos.length;
    this.exbindoFormularioTitulo = true;
  }

  mostrarCamposCheque() {
    if ( this.titulo.tipoDivida.codigo === 2 ) {
      this.cheque = true;
    } else {
      this.cheque = false;
      this.titulo.banco = null;
      this.titulo.agencia = null;
      this.titulo.contaCorrente = null;
    }
  }
  confirm() {
    this.confirmationService.confirm({
        message: 'Deseja aproveitar dados do último cadastro?',
        accept: () => {
          this.titulo = new Titulo();
          this.titulo = this.clonarTitulo(this.cliente.titulos[this.temTituloIndex - 1]);
          this.titulo.codigo = null;
          this.titulo.contratoConta = null;
          this.titulo.tipoDivida = null;
          this.titulo.numOuTituloCheque = null;
          this.titulo.banco = new Banco;
          this.titulo.contaCorrente = null;
          this.titulo.agencia = null;
          this.titulo.saldoACobrar = null;
          this.titulo.dataVencimentoDivida = null;
          this.titulo.dataEnvioEntrada = null;
          this.titulo.referencia = null;
          this.tituloIndex = this.cliente.titulos.length;
          console.log(this.titulo);
          this.exbindoFormularioTitulo = true;
        },
        reject: () => {
              this.tel2.nativeElement.value = '';
              this.tel1.nativeElement.value = '';
              this.cel.nativeElement.value = '';
              this.cepField.nativeElement.value = '';
              this.titulo = new Titulo();
              this.prepararNovoTitulo();
              this.exbindoFormularioTitulo = true;
        }
    });
  }

  showDialogToAceppt() {
    console.log(this.salTitulo.nativeElement.disabled);
    this.salTitulo.nativeElement.disabled = true;
    this.displayDialog = true;
  }

  showDialogToReject() {
    this.salTitulo.nativeElement.disabled = false;
    this.displayDialog = false;
  }

  carregarTipoDivida() {
    return this.tipoDividaService.listarTodas()
      .then(tipoDivida => {
        this.tipoDividas = tipoDivida.map(e => ({ tipo: e.tipo.toUpperCase( ), codigo: e.codigo }));
      })
      .catch(erro => this.errorHandler.handle(erro));
  }
  carregarEstados() {
    return this.estadosService.listarTodas()
      .then(estados => {
        this.estados = estados.map(e => ({ nome: e.nome.toUpperCase( ), codigo: e.codigo, uf: e.uf.toUpperCase( ) }));
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  carregarBancos() {
    return this.bancosService.listarTodas()
      .then(bancos => {
        this.bancos = bancos.map(b => ({ banco: b.banco, codigo: b.codigo }));
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  carregarCliente(codigo: number) {
    this.clienteService.buscarPorCodigo(codigo)
      .then(cliente => {
        this.cliente = cliente;

        this.temTituloIndex = this.cliente.titulos.length;
        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  atualizarTituloEdicao() {
    this.salTitulo.nativeElement.disabled = false;
    this.title.setTitle(`Cadastro de Titulos: ${this.cliente.nome}`);
  }

  confirmarTitulo(frm: FormControl) {
    if (frm.value.estados === undefined) {
    this.titulo.estado = null;
    }
    this.titulo.dataEnvioEntrada = new Date();
    this.titulo.descricao = 'carteira';
    this.cliente.titulos[this.tituloIndex] = this.clonarTitulo(this.titulo);
    this.temTituloIndex = this.cliente.titulos.length;
    this.exbindoFormularioTitulo = false;

    frm.reset();
  }

  salvar(form: FormControl) {
  /*   for (let i = 0; i < this.cliente.titulos.length; i++) {
      if (this.cliente.titulos[i].estado.nome === undefined) {
       this.cliente.titulos[i].estado = null;
      }
    } */
      this.atualizarCliente(form);
      this.temTituloIndex = this.cliente.titulos.length;
  }

  imprimirContrato(index: number) {
    this.clienteService.buscarContrato(String(index)).then(relatorio => {
      const url = window.URL.createObjectURL(relatorio);
      window.open(url);
    });
  }

  atualizarCliente(form: FormControl) {

      this.loading = true;
      this.clienteService.atualizarTitulo(this.cliente)
        .then(cliente => {
          this.toasty.success('Título incluído na cobrança com sucesso.!');
          form.reset();
          this.salTitulo.nativeElement.disabled = false;
          this.displayDialog = false;
          this.carregarCliente(cliente.codigo);
          this.loading = false;
        })
        .catch(erro => this.errorHandler.handle(erro)).then(() => {
          this.displayDialog = false;
          this.salTitulo.nativeElement.disabled = false;
          this.loading = false;
        });
      this.displayDialog = false;
  }

  prepararEdicaoTitulo(titulo: Titulo, index: number) {
    this.titulo = this.clonarTitulo(titulo);
    this.exbindoFormularioTitulo = true;
    this.tituloIndex = index;
  }

  removerTitulo(index: number) {
    this.cliente.titulos.splice(index, 1);
    this.temTituloIndex = this.cliente.titulos.length;
  }

  clonarTitulo(titulo: Titulo): Titulo {
    return new Titulo(
      titulo.codigo,
      titulo.nome, titulo.cpfCnpj,
      titulo.contratoConta, titulo.tipoDivida,
      titulo.numOuTituloCheque,
      titulo.banco, titulo.agencia, titulo.contaCorrente,
      titulo.dataEnvioEntrada, titulo.dataVencimentoDivida,
      titulo.saldoACobrar, titulo.endereco, titulo.numero,
      titulo.complemento, titulo.bairro, titulo.municipio,
      titulo.estado, titulo.cep, titulo.telefone1, titulo.telefone2,
      titulo.celular, titulo.referencia, titulo.classeLojaSetor, titulo.email,
      titulo.descricao
     );
  }

}
