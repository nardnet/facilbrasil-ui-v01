import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { ResponseContentType, Http, Headers, URLSearchParams } from '@angular/http';
import * as moment from 'moment';

@Injectable()
export class TitulosService {
  titulosUrl: string;

  constructor(private http: Http) {
       this.titulosUrl = `${environment.apiUrl}/titulos`;
   }

   upload(codigo: string, quantidade: string, saldoACobrar: string, file: File): Promise<any> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('codigo', codigo);
    formdata.append('quantidade', quantidade);
    formdata.append('saldoACobrar', saldoACobrar);

    return this.http.post(`${this.titulosUrl}/upload`, formdata)
    .toPromise()
    .then(response => response.text);
   }

   download(): Promise<any> {
    return this.http.get(`${this.titulosUrl}/download`, { responseType: ResponseContentType.Blob })
      .toPromise()
      .then(response => response.blob());
  }

}
