import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TitulosCadastroComponent } from './titulos-cadastro/titulos-cadastro.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { FileUploadModule } from 'primeng/fileupload';
import { ButtonModule } from 'primeng/components/button/button';
import {TableModule} from 'primeng/table';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {CheckboxModule} from 'primeng/checkbox';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { PanelModule } from 'primeng/panel';
import {NgxMaskModule} from 'ngx-mask';
import { LoadingModule } from 'ngx-loading';
import { SharedModule } from '../shared/shared.module';
import { TitulosConsultaComponent } from './titulos-consulta/titulos-consulta.component';
import { TitulosPrincipalComponent } from './titulos-principal/titulos-principal.component';
import { RouterModule } from '@angular/router';
import { TitulosUploadComponent } from './titulos-upload/titulos-upload.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CurrencyMaskModule,
    ConfirmDialogModule,
    FormsModule,
    InputTextModule,
    FileUploadModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    PanelModule,
    NgxMaskModule.forRoot(),
    LoadingModule.forRoot({
      fullScreenBackdrop: true,
      backdropBorderRadius: '14px'
  }),
  RouterModule
  ],
  declarations: [
    TitulosCadastroComponent,
    TitulosConsultaComponent,
    TitulosPrincipalComponent,
    TitulosUploadComponent
  ],
  exports: [
    TitulosCadastroComponent,
    TitulosPrincipalComponent
  ]
})
export class TitulosModule { }
