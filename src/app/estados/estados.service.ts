import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EstadosService {

  estadosUrl: string;


  constructor(private http: Http) {
    this.estadosUrl = `${environment.apiUrl}/estados`;

  }

  listarTodas(): Promise<any> {
    return this.http.get(this.estadosUrl)
      .toPromise()
      .then(response => response.json());
  }
}
