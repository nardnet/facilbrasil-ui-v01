import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './seguranca/auth.guard';
import { ClientesCadastroComponent } from './clientes/clientes-cadastro/clientes-cadastro.component';
import { TitulosCadastroComponent } from './titulos/titulos-cadastro/titulos-cadastro.component';
import { TitulosPrincipalComponent } from './titulos/titulos-principal/titulos-principal.component';
import { TitulosUploadComponent } from './titulos/titulos-upload/titulos-upload.component';
import { ClientesPesquisaComponent } from './clientes/clientes-pesquisa/clientes-pesquisa.component';
import { ClientesStatusComponent } from './clientes/clientes-status/clientes-status.component';
import { ClientesVideoComponent } from './clientes/clientes-video/clientes-video.component';
import { ClientesEmailsComponent } from './clientes/clientes-emails/clientes-emails.component';
import { LoginFormComponent } from './seguranca/login-form/login-form.component';
import { UsuarioCadastroComponent } from './usuario/usuario-cadastro/usuario-cadastro.component';
import { UsersistemaCadastroComponent } from './usersistema/usersistema-cadastro/usersistema-cadastro.component';
import { ChatComponent } from './atendimento/chat/chat.component';
import { MensagemComponent } from './atendimento/mensagem/mensagem.component';
import { TelefoneComponent } from './atendimento/telefone/telefone.component';
import { SuporteComponent } from './atendimento/suporte/suporte.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginFormComponent },
  { path: 'cadastro', component: ClientesCadastroComponent },
  { path: 'atendimento/chat', component: ChatComponent },
  { path: 'atendimento/mensagem', component: MensagemComponent },
  { path: 'atendimento/contato', component: TelefoneComponent },
  { path: 'atendimento/suporte', component: SuporteComponent },
  {
    path: 'usuarios/:codigoCliente',
    component: UsuarioCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'usuarioSistema',
    component: UsersistemaCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'cliente/titulos',
    component: TitulosCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: [ 'ROLE_CADASTRAR_TITULO', 'ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'cliente/status',
    component: ClientesStatusComponent,
    canActivate: [AuthGuard],
    data: { roles: [ 'ROLE_CADASTRAR_TITULO', 'ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'cliente/video',
    component: ClientesVideoComponent,
    canActivate: [AuthGuard],
    data: { roles: [ 'ROLE_CADASTRAR_TITULO', 'ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'cliente/emails',
    component: ClientesEmailsComponent,
    canActivate: [AuthGuard],
    data: { roles: [ 'ROLE_CADASTRAR_TITULO', 'ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'titulos/titulos-principal',
    component: TitulosPrincipalComponent,
    canActivate: [AuthGuard],
    data: { roles: [ 'ROLE_CADASTRAR_TITULO', 'ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'titulos/titulos-upload',
    component: TitulosUploadComponent,
    canActivate: [AuthGuard],
    data: { roles: [ 'ROLE_CADASTRAR_TITULO', 'ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'cliente/:codigoCliente',
    component: TitulosCadastroComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_CLIENTE'] }
  },
  {
    path: 'clientes',
    component: ClientesPesquisaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_PESQUISAR_CLIENTE'] }
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
