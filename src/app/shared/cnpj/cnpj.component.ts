import { FormControl } from '@angular/forms';
import { Component, Input  } from '@angular/core';
import * as cnpj from '@fnando/cnpj';

@Component({
  selector: 'app-cnpj',
  template: `
  <div *ngIf="temErro()" class="ui-message ui-messages-error">
      {{ text }}
  </div>
  `,
  styles: []
})
export class CnpjComponent  {
  @Input() error: string;
  @Input() control: FormControl;
  @Input() text: string;
  invalido: boolean;

  temErro(): boolean {
    if (this.control.dirty) {
      this.invalido = !cnpj.isValid(this.control.value);
    }
    return this.invalido;
  }

}
