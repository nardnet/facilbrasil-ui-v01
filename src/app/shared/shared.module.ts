import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageComponent } from './message/message.component';
import { CpfCnpjComponent } from './cpf-cnpj/cpf-cnpj.component';
import { CnpjComponent } from './cnpj/cnpj.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MessageComponent, CpfCnpjComponent, CnpjComponent],
  exports: [MessageComponent, CpfCnpjComponent, CnpjComponent]
})
export class SharedModule { }
