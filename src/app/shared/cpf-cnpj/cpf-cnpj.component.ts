import { FormControl } from '@angular/forms';
import { Component, Input  } from '@angular/core';
import * as cnpj from '@fnando/cnpj';
import * as cpf from '@fnando/cpf';

@Component({
  selector: 'app-cpf-cnpj',
  template: `
  <div *ngIf="temErro()" class="ui-message ui-messages-error">
      {{ text }}
  </div>
  `,
  styles: []
})
export class CpfCnpjComponent {
  @Input() error: string;
  @Input() control: FormControl;
  @Input() text: string;
  invalido: boolean;

  temErro(): boolean {
    if (this.control.dirty) {
      this.invalido = !(cnpj.isValid(this.control.value) || cpf.isValid(this.control.value));
    }
    return this.invalido;
  }

}
