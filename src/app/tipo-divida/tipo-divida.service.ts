import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoDividaService {

  tipoDividaUrl: string;

  constructor(private http: Http) {
    this.tipoDividaUrl = `${environment.apiUrl}/tipodivida`;
   }

  listarTodas(): Promise<any> {
    return this.http.get(this.tipoDividaUrl)
      .toPromise()
      .then(response => response.json());
  }
}
