import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  displayDialog: boolean;

  constructor(
    public auth: AuthService,
    private errorHandler: ErrorHandlerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  login(usuario: string, senha: string) {
    this.auth.login(usuario, senha)
      .then(() => {
        if (this.auth.jwtPayload.codigoCliente) {
          this.displayDialog = true;
         //
        } else {
          this.router.navigate(['/clientes']);
        }
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

  showDialogOK() {
    this.router.navigate(['/titulos/titulos-principal']);
  }
}
