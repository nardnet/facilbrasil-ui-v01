import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from './../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BancosService {


  bancosUrl: string;

  constructor(private http: Http) {
    this.bancosUrl  = `${environment.apiUrl}/bancos`;
  }

  listarTodas(): Promise<any> {
    return this.http.get(this.bancosUrl)
      .toPromise()
      .then(response => response.json());
  }
}
