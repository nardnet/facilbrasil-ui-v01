import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../../seguranca/auth.service';
import { LogoutService } from '../../seguranca/logout.service';
import { ErrorHandlerService } from '../error-handler.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
     private router: Router,
     private auth: AuthService,
     private logoutService: LogoutService,
     private errorHandler: ErrorHandlerService
    ) { }

  ngOnInit() {
  //   $(document).ready(function () {
  //     $('nav').find('.container').on('click', 'a', function () {
  //         $('.navbar-menu').show();
  //     });

  //     $('nav').find('li').on('click', 'a', function () {
  //       $('.navbar-menu').hide();
  //     });
  // });
  }

  exibindoNavbar() {
    return ((this.router.url !== '/login') && (this.router.url !== '/cadastro'));
  }

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
