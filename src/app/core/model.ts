export class Estado {
  codigo: number;
  nome: string;
  uf: string;
}

export class Banco {
  codigo: number;
  banco: string;
}

export class TipoDivida {
  codigo: number;
  tipo: string;
}

export class AtendimentoContato {
  codigo: number;
  nome: string;
  telefone: string;
  cpfCnpj: string;
  horario: string;
  codigoCliente: number;

  constructor(
    codigo?: number,
    nome?: string,
    telefone?: string,
    cpfCnpj?: string,
    horario?: string,
    codigoCliente?: number
  ) {
    this.codigo = codigo,
    this.nome = nome,
    this.telefone = telefone,
    this.cpfCnpj = cpfCnpj,
    this.horario = horario,
    this.codigoCliente = codigoCliente;
  }

}

export class Mensagem {
  codigo: number;
  nome: string;
  email: string;
  assunto: string;
  mensagem: string;
  codigoCliente: number;

  constructor(
    codigo?: number,
    nome?: string,
    email?: string,
    assunto?: string,
    mensagem?: string,
    codigoCliente?: number
  ) {
    this.codigo = codigo,
    this.nome = nome,
    this.email = email,
    this.assunto = assunto,
    this.mensagem = mensagem,
    this.codigoCliente = codigoCliente;

  }
}

export class Titulo {
codigo: number;
nome: string;
cpfCnpj: string;
contratoConta: string;
tipoDivida = new TipoDivida();
numOuTituloCheque: string;
banco = new Banco();
agencia: string;
contaCorrente: string;
dataEnvioEntrada: Date;
dataVencimentoDivida: Date;
saldoACobrar: number;
endereco: string;
numero: string;
complemento: string;
bairro: string;
municipio: string;
estado = new Estado();
cep: string;
telefone1: string;
telefone2: string;
celular: string;
referencia: string;
classeLojaSetor: string;
email: string;
descricao: string;

constructor(
codigo?: number,
nome?: string,
cpfCnpj?: string,
contratoConta?: string,
tipoDivida?: TipoDivida,
numOuTituloCheque?: string,
banco?: Banco,
agencia?: string,
contaCorrente?: string,
dataEnvioEntrada?: Date,
dataVencimentoDivida?: Date,
saldoACobrar?: number,
endereco?: string,
numero?: string,
complemento?: string,
bairro?: string,
municipio?: string,
estado?: Estado,
cep?: string,
telefone1?: string,
telefone2?: string,
celular?: string,
referencia?: string,
classeLojaSetor?: string,
email?: string,
descricao?: string
) {
  this.codigo = codigo,
  this.nome = nome,
  this.cpfCnpj = cpfCnpj,
  this.contratoConta = contratoConta,
  this.tipoDivida = tipoDivida,
  this.numOuTituloCheque = numOuTituloCheque,
  this.banco = banco,
  this.agencia = agencia,
  this.contaCorrente = contaCorrente,
  this.dataEnvioEntrada = dataEnvioEntrada,
  this.dataVencimentoDivida = dataVencimentoDivida,
  this.saldoACobrar = saldoACobrar,
  this.endereco = endereco,
  this.numero = numero,
  this.complemento = complemento,
  this.bairro = bairro,
  this.municipio = municipio,
  this.estado = estado,
  this.cep = cep,
  this.telefone1 = telefone1,
  this.telefone2 = telefone2,
  this.celular = celular,
  this.referencia = referencia,
  this.classeLojaSetor = classeLojaSetor,
  this.email = email,
  this.descricao = descricao;
}
}
export class Usuario {
  codigo: number;
  usuario: string;
  email: string;
  senha: string;
  permissoes = new Array<Permissao>();

  constructor(
    codigo?: number,
    usuario?: string,
    email?: string,
    senha?: string,
    permissoes?: Permissao[]
  ) {
    this.codigo = codigo;
    this.usuario = usuario;
    this.email = email;
    this.senha = senha;
    this.permissoes = permissoes;
  }
}


export class Permissao {
  codigo: number;
  descricao: string;
}


export class Endereco {
  endereco: string;
  numero: string;
  bairro: string;
  cidade: string;
  estado = new Estado();
  cep: string;
}

export class Clientes {

  codigo: number;
  nome: string;
  email: string;
  cpf: string;
  rzoSocial: string;
  cnpj: string;
  inscEstadual: string;
  endereco = new Endereco();
  telefone: string;
  whatsapp: string;
  strAtividade: string;
  princConcorrentes: string;
  indInadimplencia: string;
  empGroup: string;
  contCobranca: string;
  cargo: string;
  repNome: string;
  repCpf: string;
  repEmail: string;
  observacoes: string;
  titulos = new Array<Titulo>();
  banco = new Banco();
  agencia: string;
  contaCorrente: string;
  operacao: string;
  dtCadastro: Date;
  usuario =  new Array<Usuario>();
  ativo = true;
}
