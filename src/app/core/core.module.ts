import { RouterModule } from '@angular/router';
import { NgModule, LOCALE_ID  } from '@angular/core';
import { CommonModule, registerLocaleData  } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { NavbarComponent } from './navbar/navbar.component';
declare var jQuery;
import { UsersistemaService } from './../usersistema/usersistema.service';
import { ClienteService } from './../clientes/cliente.service';
import { TitulosService } from './../titulos/titulos.service';
import { UsuarioService } from '../usuario/usuario.service';
import { AtendimentoService } from '../atendimento/atendimento.service';
import {ConfirmationService} from 'primeng/api';
import { AuthService } from './../seguranca/auth.service';
import { ErrorHandlerService } from './error-handler.service';
import { LoadingModule } from 'ngx-loading';
import { JwtHelper } from 'angular2-jwt';
import { ToastyModule } from 'ng2-toasty';
import { NaoAutorizadoComponent } from './nao-autorizado.component';

registerLocaleData(localePt);

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ToastyModule.forRoot(),
    LoadingModule
  ],
  declarations: [
    NavbarComponent,
    NaoAutorizadoComponent
  ],
  exports: [NavbarComponent,  ToastyModule, RouterModule, LoadingModule],
  providers: [
    ConfirmationService,
    ErrorHandlerService,
    AuthService,
    ClienteService,
    UsuarioService,
    TitulosService,
    AtendimentoService,
    UsersistemaService,
    JwtHelper,
    { provide: LOCALE_ID, useValue: 'pt' }

  ]

})
export class CoreModule { }
