import { UsuarioModule } from './usuario/usuario.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoadingModule } from 'ngx-loading';
import { AppComponent } from './app.component';
import { ClientesModule } from './clientes/clientes.module';
import { AtendimentoModule } from './atendimento/atendimento.module';
import { TitulosModule } from './titulos/titulos.module';
declare var jQuery;
import { CoreModule } from './core/core.module';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { SegurancaModule } from './seguranca/seguranca.module';
import { UsersistemaModule } from './usersistema/usersistema.module';

@NgModule({
  declarations: [
    AppComponent,
    ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ClientesModule,
    AtendimentoModule,
    SegurancaModule,
    TitulosModule,
    UsuarioModule,
    LoadingModule,
    UsersistemaModule,
    AppRoutingModule,
    CoreModule,
    HttpModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
