import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UsuarioCadastroComponent } from './usuario-cadastro/usuario-cadastro.component';
import {NgxMaskModule} from 'ngx-mask';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ButtonModule } from 'primeng/components/button/button';
import {TableModule} from 'primeng/table';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {CheckboxModule} from 'primeng/checkbox';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { PanelModule } from 'primeng/panel';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxMaskModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    DialogModule,
    ConfirmDialogModule,
    CheckboxModule,
    DropdownModule,
    PanelModule,
    SharedModule
  ],
  declarations: [UsuarioCadastroComponent],
  exports: [UsuarioCadastroComponent]
})
export class UsuarioModule { }
