import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Usuario, Clientes } from '../../core/model';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { ToastyService } from 'ng2-toasty';
import { ClienteService } from '../../clientes/cliente.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html',
  styleUrls: ['./usuario-cadastro.component.css']
})
export class UsuarioCadastroComponent implements OnInit {
  permissoes = [
    { codigo: 1, descricao: 'ROLE_CADASTRAR_TITULO'}
  ];
  usuario;
  cliente = new Clientes();
  usuarioIndex: number;
  exbindoFormularioUsuario = false;
  @ViewChild('salvaUser') salvarUsuario: ElementRef;
  displayDialog: boolean;

  constructor(
    private clienteService: ClienteService,
    private errorHandler: ErrorHandlerService,
    private toasty: ToastyService,
    private route: ActivatedRoute,

  ) { }

  ngOnInit() {
    this.usuario = new Usuario();
    const codigoCliente = this.route.snapshot.params['codigoCliente'];
    if (codigoCliente) {
      this.carregarCliente(codigoCliente);
    }
  }

  confirmarUsuario(frm: FormControl) {
    this.usuario.permissoes = this.permissoes;
    this.usuario.email = this.cliente.email;
    this.cliente.usuario[this.usuarioIndex] = this.clonarUsuario(this.usuario);
    this.exbindoFormularioUsuario = false;
    this.salvarUsuario.nativeElement.disabled = false;
    frm.reset();
  }

  showDialogToAceppt() {
    console.log(this.salvarUsuario.nativeElement.disabled);
    this.salvarUsuario.nativeElement.disabled = true;
    this.displayDialog = true;
  }

  showDialogToReject() {
    this.salvarUsuario.nativeElement.disabled = true;
    this.displayDialog = false;
  }

  prepararNovoUsuario() {
    this.exbindoFormularioUsuario = true;
    this.usuario = new Usuario();
    this.usuarioIndex = this.cliente.usuario.length;

  }

  prepararEdicaoUsuario(usuario: Usuario, index: number) {
    this.usuario = this.clonarUsuario(usuario);
    this.usuario.senha = null;
    this.exbindoFormularioUsuario = true;
    this.usuarioIndex = index;
  }

  removerUsuario(index: number) {
    this.cliente.usuario.splice(index, 1);

  }

  salvar(form: FormControl) {
        this.atualizarCliente(form);
    }

    atualizarCliente(form: FormControl) {
      console.log(this.cliente);
       this.clienteService.atualizar(this.cliente)
        .then(cliente => {
          this.cliente = cliente;
          this.salvarUsuario.nativeElement.disabled = false;
          this.toasty.success('Usuario alterado com sucesso!');
        })
        .catch(erro => this.errorHandler.handle(erro)).then(() => {
          this.salvarUsuario.nativeElement.disabled = false;
        });
        this.displayDialog = false;
    }

  clonarUsuario(usuario: Usuario): Usuario {
    return new Usuario(
      usuario.codigo,
      usuario.usuario,
      usuario.email,
      usuario.senha,
      usuario.permissoes );
}
  carregarCliente(codigo: number) {
    this.clienteService.buscarPorCodigo(codigo)
      .then(cliente => {
        this.cliente = cliente;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
