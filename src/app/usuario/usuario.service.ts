import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { Usuario } from '../core/model';
import { Http, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuarioUrl: string;
  constructor(private http: Http) {
    this.usuarioUrl = `${environment.apiUrl}/usuarios`;
   }

   adicionar(usuario: Usuario): Promise<Usuario> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.usuarioUrl,
        JSON.stringify(usuario), { headers })
      .toPromise()
      .then(response => response.json());
  }

  atualizar(usuario: Usuario): Promise<Usuario> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(`${this.usuarioUrl}/atualizar`,
        JSON.stringify(usuario), { headers })
      .toPromise()
      .then(response => response.json() as Usuario);
  }

  buscarPorCodigoCliente(codigo: number): Promise<Usuario> {
    return this.http.get(`${this.usuarioUrl}/usuarioCliente/${codigo}`)
      .toPromise()
      .then(response => response.json() as Usuario);
  }


  buscarPorCodigo(codigo: number): Promise<Usuario> {
    return this.http.get(`${this.usuarioUrl}/${codigo}`)
      .toPromise()
      .then(response => response.json() as Usuario);
  }


}
