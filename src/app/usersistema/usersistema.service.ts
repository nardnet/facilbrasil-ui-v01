import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from './../../environments/environment';
import { Usuario } from '../core/model';
@Injectable({
  providedIn: 'root'
})
export class UsersistemaService {

  usuarioSistemaUrl: string;

  constructor(private http: Http) {
    this.usuarioSistemaUrl  = `${environment.apiUrl}/usuarios`;
   }

   adicionar(usuario: Usuario): Promise<Usuario> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.usuarioSistemaUrl,
        JSON.stringify(usuario), { headers })
      .toPromise()
      .then(response => response.json());
  }

   atualizar(usuario: Usuario): Promise<Usuario> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(`${this.usuarioSistemaUrl}/${usuario.codigo}`,
        JSON.stringify(usuario), { headers })
      .toPromise()
      .then(response => response.json() as Usuario);
  }

   listarTodas(): Promise<any> {
    return this.http.get(`${this.usuarioSistemaUrl}/usuarioSistema`)
      .toPromise()
      .then(response => response.json());
  }

  excluir(codigo: number): Promise<void> {
    return this.http.delete(`${this.usuarioSistemaUrl}/${codigo}`)
      .toPromise()
      .then(() => null);
  }

}
