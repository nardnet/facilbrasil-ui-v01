import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { ToastyService } from 'ng2-toasty';
import { ActivatedRoute } from '@angular/router';
import { UsersistemaService } from '../usersistema.service';
import { Usuario } from '../../core/model';

@Component({
  selector: 'app-usersistema-cadastro',
  templateUrl: './usersistema-cadastro.component.html',
  styleUrls: ['./usersistema-cadastro.component.css']
})
export class UsersistemaCadastroComponent implements OnInit {
  permissoes = [
    { codigo: 6, descricao: 'ROLE_PESQUISAR_CLIENTE'}
  ];
  usuario;
  editando: boolean;
  usuarioSelecionado;
  usuarioIndex: number;
  exbindoFormularioUsuario = false;
  @ViewChild('tabela') grid;
  constructor(
    private usersistemaService: UsersistemaService,
    private errorHandler: ErrorHandlerService,
    private toasty: ToastyService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.usuarioSelecionado = new Usuario();
    this.usuario = [];
    this.carregarUsuarios();
  }

  prepararNovoUsuario() {
    this.editando = false;
    this.exbindoFormularioUsuario = true;
    this.usuarioSelecionado = new Usuario();
    this.usuarioIndex = this.usuario.length;

  }

  prepararEdicaoUsuario(usuario: Usuario, index: number) {
    this.editando = true;
    this.usuarioSelecionado  = this.clonarUsuario(usuario);
    this.usuarioSelecionado.senha = null;
    this.exbindoFormularioUsuario = true;
    this.usuarioIndex = index;
  }

  removerUsuario(usuario: any) {
    this.excluir(usuario);
  }

  clonarUsuario(usuario: Usuario): Usuario {
    return new Usuario(
      usuario.codigo,
      usuario.usuario,
      usuario.email,
      usuario.senha,
      usuario.permissoes );
}

salvar(form: FormControl) {
  if (this.editando) {
    this.atualizarUsuario(form);
    console.log('atualizar');
  } else {
    console.log('adcionar');
    this.adicionarLancamento(form);
  }
}
adicionarLancamento(form: FormControl) {
  this.usuarioSelecionado.permissoes = this.permissoes;
  this.usersistemaService.adicionar(this.usuarioSelecionado)
    .then(usuario => {
      this.toasty.success('Usuario adicionado com sucesso!');
      this.carregarUsuarios();
      form.reset();
      this.usuarioSelecionado = new Usuario();
      this.exbindoFormularioUsuario = false;
    })
    .catch(erro => this.errorHandler.handle(erro));
}

atualizarUsuario(form: FormControl) {
this.usuarioSelecionado.permissoes = this.permissoes;
 this.usersistemaService.atualizar(this.usuarioSelecionado)
  .then(usuario => {
    this.carregarUsuarios();
    form.reset();
    this.toasty.success('Usuario alterado com sucesso!');
    this.exbindoFormularioUsuario = false;
  })
  .catch(erro => this.errorHandler.handle(erro));
}

  carregarUsuarios() {
    this.usersistemaService.listarTodas()
      .then(usuario => {
        this.usuario = usuario;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  excluir(usuario: any) {
    this.usersistemaService.excluir(usuario.codigo)
      .then(() => {
        if (this.grid.first === 0) {
          this.carregarUsuarios();
        } else {
          this.grid.first = 0;
        }

        this.toasty.success('Usuário excluído com sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }


}
