import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ButtonModule } from 'primeng/components/button/button';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { NgxMaskModule } from 'ngx-mask';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

import { LoadingModule } from 'ngx-loading';

import { ChatComponent } from './chat/chat.component';
import { MensagemComponent } from './mensagem/mensagem.component';
import { TelefoneComponent } from './telefone/telefone.component';
import { SuporteComponent } from './suporte/suporte.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    NgxMaskModule.forRoot(),
    SharedModule,
    RouterModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true,
      backdropBorderRadius: '14px'
  })
  ],
  declarations: [
    ChatComponent,
    MensagemComponent,
    TelefoneComponent,
    SuporteComponent
  ],
  exports: [
    ChatComponent,
    MensagemComponent,
    TelefoneComponent
  ]
})
export class AtendimentoModule { }
