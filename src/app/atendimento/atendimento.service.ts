import { AtendimentoContato, Clientes } from './../core/model';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { ResponseContentType, Http, Headers, URLSearchParams } from '@angular/http';
import * as moment from 'moment';
import { Mensagem } from '../core/model';


@Injectable()
export class AtendimentoService {
  atendimentoUrl: string;

  constructor(private http: Http) {
    this.atendimentoUrl = `${environment.apiUrl}/atendimento`;

   }

   salvarMensagem(mensagem: Mensagem): Promise<Mensagem> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.atendimentoUrl}/mensagem`,
      JSON.stringify(mensagem), { headers })
    .toPromise()
    .then(response => response.json());
}

  salvarContato(contato: AtendimentoContato): Promise<AtendimentoContato> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.atendimentoUrl}/contato`,
      JSON.stringify(contato), { headers })
    .toPromise()
    .then(response => response.json());
  }

  listarMsg(codigo: Number): any {
    return this.http.get(`${this.atendimentoUrl}/mensagens/${codigo}`)
    .toPromise()
    .then(response => response.json());
  }
}


