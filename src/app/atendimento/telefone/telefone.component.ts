import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { ToastyService } from 'ng2-toasty';
import { ActivatedRoute } from '@angular/router';
import { AtendimentoContato } from '../../core/model';
import { AtendimentoService } from '../atendimento.service';
import { AuthService } from '../../seguranca/auth.service';

@Component({
  selector: 'app-telefone',
  templateUrl: './telefone.component.html',
  styleUrls: ['./telefone.component.css']
})
export class TelefoneComponent implements OnInit {
  contato = new AtendimentoContato();
  loading = false;
  constructor(
    private atendimentoService: AtendimentoService,
    private errorHandler: ErrorHandlerService,
    private toasty: ToastyService,
    private auth: AuthService,
    private route: ActivatedRoute,
  ) {

   }

  ngOnInit() {
    const codAuth = this.auth.jwtPayload.codigoCliente;
    const codigoCliente = this.route.snapshot.params['codigoCliente'];

    if (codAuth) {
      this.contato.codigoCliente = codAuth;
    } else if (codigoCliente) {
      this.contato.codigoCliente = codAuth;
    }

  }
  salvarContato(form: FormControl) {
    this.loading = true;
     this.atendimentoService.salvarContato(this.contato)
      .then(contato => {
        this.toasty.success('Sua mensagem foi enviada! Aguarde o contato da nossa equipe!');
        this.loading = false;
        form.reset();
      })
      .catch(erro => this.errorHandler.handle(erro)).then(() => {
        this.loading = false;
      });
  }
}
