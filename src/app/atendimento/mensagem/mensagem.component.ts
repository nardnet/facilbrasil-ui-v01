import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { ToastyService } from 'ng2-toasty';
import { ActivatedRoute } from '@angular/router';
import { Mensagem } from '../../core/model';
import { AtendimentoService } from '../atendimento.service';
import { AuthService } from '../../seguranca/auth.service';

@Component({
  selector: 'app-mensagem',
  templateUrl: './mensagem.component.html',
  styleUrls: ['./mensagem.component.css']
})
export class MensagemComponent implements OnInit {
  msg = new Mensagem();
  loading = false;
  constructor(
    private atendimentoService: AtendimentoService,
    private errorHandler: ErrorHandlerService,
    private toasty: ToastyService,
    private auth: AuthService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    const codAuth = this.auth.jwtPayload.codigoCliente;
    const codigoCliente = this.route.snapshot.params['codigoCliente'];

    if (codAuth) {
      this.msg.codigoCliente = codAuth;
    } else if (codigoCliente) {
      this.msg.codigoCliente = codAuth;
    }

  }
  salvarMensagem(form: FormControl) {
    this.loading = true;
     this.atendimentoService.salvarMensagem(this.msg)
      .then(mensagem => {
        this.toasty.success('Sua mensagem foi enviada! Aguarde o contato da nossa equipe!');
        this.loading = false;
        form.reset();
      })
      .catch(erro => this.errorHandler.handle(erro)).then(() => {
        this.loading = false;
      });
  }

}
