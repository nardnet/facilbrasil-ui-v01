import { Component, OnInit } from '@angular/core';
import { Clientes, Mensagem } from '../../core/model';
import { ClienteService } from '../cliente.service';
import { ToastyConfig } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../seguranca/auth.service';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { AtendimentoService } from '../../atendimento/atendimento.service';

@Component({
  selector: 'app-clientes-emails',
  templateUrl: './clientes-emails.component.html',
  styleUrls: ['./clientes-emails.component.css']
})
export class ClientesEmailsComponent implements OnInit {
  msgs = new Array<Mensagem>();
  codAuth: number;

  constructor(
    private atendimentoService: AtendimentoService,
    private clienteService: ClienteService,
    private toastyConfig: ToastyConfig,
    private router: Router,
    public auth: AuthService,
    private errorHandler: ErrorHandlerService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.codAuth = this.auth.jwtPayload.codigoCliente;
    const codigoCliente = this.route.snapshot.params['codigoCliente'];
    // tslint:disable-next-line:no-debugger
    debugger;
    if (this.codAuth) {
      this.carregarMensagens();
    } else if (codigoCliente) {
      this.codAuth = codigoCliente;
      this.carregarMensagens();
    }
  }

  carregarMensagens() {
    return this.atendimentoService.listarMsg(this.codAuth)
      .then(msg => {

        this.msgs = msg;

      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
