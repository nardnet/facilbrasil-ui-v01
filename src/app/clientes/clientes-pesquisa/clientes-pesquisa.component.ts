import { ClienteService, ClienteFiltro } from './../cliente.service';
import { Component, OnInit, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import { LazyLoadEvent } from 'primeng/components/common/api';
import { ToastyService } from 'ng2-toasty';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { Usuario } from '../../core/model';


@Component({
  selector: 'app-clientes-pesquisa',
  templateUrl: './clientes-pesquisa.component.html',
  styleUrls: ['./clientes-pesquisa.component.css']
})
export class ClientesPesquisaComponent implements OnInit, AfterContentChecked {
  usuario = new Usuario();
  usuarioBanco = new Usuario();
  totalRegistros = 0;
  filtro = new ClienteFiltro();
  nomeUsuario = '';
  codUsuario = '';
  clientes = [ ];
  pt = {};
  displayDialog: boolean;
  loading = false;

  constructor(
    private errorHandler: ErrorHandlerService,
    private clienteService: ClienteService,
    private toasty: ToastyService,
    private cdref: ChangeDetectorRef
  ) { }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  ngOnInit() {
   // this.pesquisar();
   this.pt = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
      'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    today: 'Hoje',
    clear: 'Limpar'
  };
  }

  gerarTitulos(index: string) {
    this.loading = true;
    console.log('Cliente codigo: ' + index);
    this.clienteService.gerarTitulos(index) .then(relatorio => {
      const url = window.URL.createObjectURL(relatorio);
      this.loading = false;
      window.open(url);
    }).catch(erro => this.errorHandler.handle(erro)).then(() => {
      this.loading = false;
    });
  }

  alternarStatus(cliente: any): void {
    const novoStatus = !cliente.ativo;
    this.loading = true;
    this.clienteService.mudarStatus(cliente.codigo, novoStatus)
      .then(() => {
        const acao = novoStatus ? 'ativada' : 'desativada';

        cliente.ativo = novoStatus;
        this.loading = false;
        this.toasty.success(`Cliente ${acao} com sucesso!`);
      })
      .catch(erro => this.errorHandler.handle(erro)).then(() => {
        this.loading = false;
      });
  }

  imprimirContrato(index: string) {
    this.loading = true;

    this.clienteService.buscarContrato(index) .then(relatorio => {
      const url = window.URL.createObjectURL(relatorio);
      this.loading = false;
      window.open(url);
    }).catch(erro => this.errorHandler.handle(erro)).then(() => {
      this.loading = false;
    });
  }

 pesquisar(pagina = 0) {
  this.loading = true;
  this.filtro.pagina = pagina;
  this.clienteService.pesquisar(this.filtro)
  .then(resultado => {
    this.totalRegistros = resultado.total;
    this.clientes = resultado.clientes;
    this.loading = false;
  }).catch(erro => this.errorHandler.handle(erro)).then(() => {
    this.loading = false;
  });
 }

 aoMudarPagina(event: LazyLoadEvent) {
  const pagina = event.first / event.rows;
  this.pesquisar(pagina);
}

 limparFiltros() {
   this.filtro = new ClienteFiltro();
  }

 showDialogToAdd(cliente: any) {
  this.displayDialog = true;
}


clonarUsuario(usuario: Usuario): Usuario {
  return new Usuario(
    usuario.codigo,
    usuario.usuario, usuario.email, usuario.senha,
   );
}
}
