import { Component, OnInit } from '@angular/core';
import { ToastyConfig } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../seguranca/auth.service';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { ClienteService } from '../cliente.service';
import { Clientes } from '../../core/model';

@Component({
  selector: 'app-clientes-status',
  templateUrl: './clientes-status.component.html',
  styleUrls: ['./clientes-status.component.css']
})
export class ClientesStatusComponent implements OnInit {
  cliente = new Clientes();
  constructor(
    private clienteService: ClienteService,
    private toastyConfig: ToastyConfig,
    private router: Router,
    public auth: AuthService,
    private errorHandler: ErrorHandlerService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    const codAuth = this.auth.jwtPayload.codigoCliente;
    const codigoCliente = this.route.snapshot.params['codigoCliente'];
    if (codAuth) {
      this.carregarCliente(codAuth);
    } else if (codigoCliente) {
      this.carregarCliente(codigoCliente);
    }
  }
  carregarCliente(codigo: number) {
    this.clienteService.buscarPorCodigo(codigo)
      .then(cliente => {
        this.cliente = cliente;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }
}
