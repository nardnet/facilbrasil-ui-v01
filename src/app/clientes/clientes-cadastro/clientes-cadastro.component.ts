import { ClienteService } from './../cliente.service';
import { Component, OnInit, ViewChild, ElementRef, OnChanges, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import { FormControl  } from '@angular/forms';
import { BancosService } from '../../bancos/bancos.service';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { EstadosService } from '../../estados/estados.service';
import { Clientes, Banco, Estado, Titulo } from '../../core/model';
import { ToastyService } from 'ng2-toasty';
import * as $ from 'jquery';

@Component({
  selector: 'app-clientes-cadastro',
  templateUrl: './clientes-cadastro.component.html',
  styleUrls: ['./clientes-cadastro.component.css']
})
export class ClientesCadastroComponent implements OnInit, OnChanges, AfterContentChecked {
 @ViewChild('campoConfirmar') confirmar: ElementRef;

  loading = false;
  sucesso: boolean;
  displayDialog: boolean;
  checked: boolean;
  info: boolean;
  obrNome: boolean;
  obrEmail: boolean;
  obrrazSocial: boolean;
  obrCnpj: boolean;
  obrInscEstadual: boolean;
  obrEndereco: boolean;
  obrNumero: boolean;
  obrBairro: boolean;
  obrCidade: boolean;
  obrEstado: boolean;
  obrCep: boolean;
  obrTelefone: boolean;
  obrwhatsapp: boolean;
  obrSetAtiv: boolean;
  obrBanco: boolean;
  obrAgencia: boolean;
  obrCC: boolean;

  pessoa = [
    { label: 'PESSOA FÍSICA', value: '1' },
    { label: 'PESSOA JÚRIDICA', value: '2' },
  ];
  estados = new Estado();
  bancos = new Banco();
  clientes = new Clientes();


  constructor(
    private clienteService: ClienteService,
    private bancosService: BancosService,
    private estadosService: EstadosService,
    private errorHandler: ErrorHandlerService,
    private toasty: ToastyService,
    private cdref: ChangeDetectorRef
  ) {}

  ngAfterContentChecked() {
    this.cdref.detectChanges();

  }

  ngOnInit() {
    this.checked = false,
    this.carregarBancos();
    this.carregarEstados();
  }

   ngOnChanges() {
    console.log('entrou aqui!');
  }

  carregarEstados() {
    return this.estadosService.listarTodas()
      .then(estados => {
        this.estados = estados.map(e => ({ nome: e.nome.toUpperCase( ), codigo: e.codigo, uf: e.uf.toUpperCase( ) }));
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  carregarBancos() {
    return this.bancosService.listarTodas()
      .then(bancos => {
        this.bancos = bancos.map(b => ({ banco: b.banco, codigo: b.codigo }));
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

 showDialogToAceppt(event) {
  this.confirmar.nativeElement.disabled = true;
  if (this.clientes.nome) {
      this.obrNome = false;
  } else {
    this.obrNome = true;
  }
  if (this.clientes.email) {
     this.obrEmail = false;
   } else {
     this.obrEmail = true;
   }
   if (this.clientes.rzoSocial) {
     this.obrrazSocial = false;
   } else {
     this.obrrazSocial = true;
   }
   if (this.clientes.cnpj) {
     this.obrCnpj = false;
   } else {
     this.obrCnpj = true;
   }
    if (this.clientes.inscEstadual) {
     this.obrInscEstadual = false;
   } else {
     this.obrInscEstadual = true;
   }
   if (this.clientes.endereco.endereco) {
     this.obrEndereco = false;
   } else {
     this.obrEndereco = true;
   }
   if (this.clientes.endereco.numero) {
     this.obrNumero = false;
   } else {
     this.obrNumero = true;
   }
   if (this.clientes.endereco.bairro) {
     this.obrBairro = false;
   } else {
     this.obrBairro = true;
   }
   if (this.clientes.endereco.cidade) {
     this.obrCidade = false;
   } else {
     this.obrCidade = true;
   } if (Object.keys(this.clientes.endereco.estado).length !== 0) {
     console.log(Object.keys(this.clientes.endereco.estado).length);
    this.obrEstado = false;
  } else {
    this.obrEstado = true;
  }
    if (this.clientes.endereco.cep) {
     this.obrCep = false;
   } else {
     this.obrCep = true;
   }
   if (this.clientes.telefone) {
     this.obrTelefone = false;
   } else {
     this.obrTelefone = true;
   }
   if (this.clientes.whatsapp) {
     this.obrwhatsapp = false;
   } else {
     this.obrwhatsapp = true;
   }
   if (this.clientes.strAtividade) {
     this.obrSetAtiv = false;
   } else {
     this.obrSetAtiv = true;
   }
   if (Object.keys(this.clientes.banco).length !== 0) {
    this.obrBanco = false;
  } else {
    this.obrBanco = true;
  } if (this.clientes.agencia) {
    this.obrAgencia = false;
  } else {
    this.obrAgencia = true;
  }if (this.clientes.contaCorrente) {
    this.obrCC = false;
  } else {
    this.obrCC = true;
  }

   if (this.obrNome  ||  this.obrEmail || this.obrrazSocial ||
      this.obrCnpj || this.obrInscEstadual  ||  this.obrEndereco ||
      this.obrNumero || this.obrNumero || this.obrBairro || this.obrCidade ||
      this.obrEstado || this.obrCep || this.obrTelefone || this.obrwhatsapp || this.obrSetAtiv ) {
        this.info = true;
        window.scroll(0, 200);
   } else if (this.obrBanco || this.obrAgencia || this.obrCC) {
        this.info = true;
        window.scrollTo(0 , document.body.scrollHeight);
   } else {
      this.info = false;
      this.displayDialog = true;
   }


}

imprimirContrato(cliente: Clientes) {
  this.clienteService.gerarContrato(cliente).then(relatorio => {
    const url = window.URL.createObjectURL(relatorio);
      window.open(url);
    }).catch(erro => this.errorHandler.handle(erro)).then(() => {
      this.loading = false;
    });
}

showDialogToReject(event) {
   this.loading = false;
  this.confirmar.nativeElement.disabled = false;
  this.displayDialog = false;
  location.reload();
}

  salvar(form: FormControl) {
    this.loading = true;
    if (this.displayDialog) {
      this.clienteService.adicionar(this.clientes)
      .then(() => {
        this.loading = false;
        this.toasty.success('Cadastro adicionado com sucesso!');
        window.scrollTo(0 , 0);
        form.reset();
        this.clientes = new Clientes();
        this.sucesso = true;
        this.confirmar.nativeElement.disabled = false;
      })
      .catch(erro => this.errorHandler.handle(erro)).then(() => {
        this.loading = false;
        this.confirmar.nativeElement.disabled = false;
      });
    }
    this.displayDialog = false;
   }

}
