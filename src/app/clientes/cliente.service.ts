import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { ResponseContentType, Http, Headers, URLSearchParams } from '@angular/http';
import * as moment from 'moment';
import { Clientes } from '../core/model';


export class ClienteFiltro {
  nome: string;
  cpf: string;
  telefone: string;
  dataCadastroInicio: Date;
  dataCadastroFim: Date;
  inativo:  boolean;
  pagina = 0;
  itensPorPagina = 10;
}

@Injectable()
export class ClienteService {
  clientesUrl: string;
  titulosUrl: string;

  constructor(private http: Http) {
    this.clientesUrl = `${environment.apiUrl}/clientes`;
    this.titulosUrl = `${environment.apiUrl}/titulos`;
   }

  pesquisar(filtro: ClienteFiltro): Promise<any> {
   const params = new URLSearchParams();

    params.set('page', filtro.pagina.toString());
    params.set('size', filtro.itensPorPagina.toString());

    if (filtro.nome) {
      params.set('nome', filtro.nome);
    }
    if (filtro.cpf) {
      params.set('cpf', filtro.cpf);
    }
    if (filtro.telefone) {
      params.set('telefone', filtro.telefone);
    }

    if (filtro.inativo) {
      params.set('inativo', String(filtro.inativo));
    }

    if (filtro.dataCadastroInicio) {
      params.set('dataCadastroDe',
      moment(filtro.dataCadastroInicio).format('DD/MM/YYYY'));
    }

    if (filtro.dataCadastroFim) {
      params.set('dataCadastroAte',
        moment(filtro.dataCadastroFim).format('DD/MM/YYYY'));
    }
    return this.http.get(`${this.clientesUrl}?resumo`, { search: params })
      .toPromise()
      .then(response => {
        const responseJson = response.json();
        const clientes = responseJson.content;

        const resultado = {
          clientes,
          total: responseJson.totalElements
        };
        return resultado;
         }
       );
  }

  atualizar(cliente: Clientes): Promise<Clientes> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(`${this.clientesUrl}/${cliente.codigo}`,
        JSON.stringify(cliente), { headers })
      .toPromise()
      .then(response => response.json() as Clientes);
  }

  atualizarTitulo(cliente: Clientes): Promise<Clientes> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(`${this.titulosUrl}/${cliente.codigo}`,
        JSON.stringify(cliente), { headers })
      .toPromise()
      .then(response => response.json() as Clientes);
  }

  mudarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(`${this.clientesUrl}/ativo/${codigo}`, ativo, { headers })
      .toPromise()
      .then(() => null);
  }


  adicionar(cliente: Clientes): Promise<Clientes> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    cliente.dtCadastro =  new Date();
    return this.http.post(this.clientesUrl,
        JSON.stringify(cliente), { headers })
      .toPromise()
      .then(response => response.json());
  }

  buscarPorCodigo(codigo: number): Promise<Clientes> {
    return this.http.get(`${this.clientesUrl}/${codigo}`)
      .toPromise()
      .then(response => response.json() as Clientes);
  }

  buscarContrato(codigo: string) {
    const params = new URLSearchParams();
    params.set('codigo', codigo );

    return this.http.get(`${this.clientesUrl}/contrato/cliente`,
      { search: params, responseType: ResponseContentType.Blob })
      .toPromise()
      .then(response => response.blob());
  }

  gerarContrato(cliente: Clientes) {

    return this.http.post(`${this.clientesUrl}/visualizar/contrato`,
        JSON.stringify(cliente), {responseType: ResponseContentType.Blob})
      .toPromise()
      .then(response => response.blob());
  }

  gerarTitulos(codigo: string) {
    const params = new URLSearchParams();
    params.set('codigo', codigo );

    return this.http.get(`${this.clientesUrl}/titulos/cliente`,
      { search: params, responseType: ResponseContentType.Blob })
      .toPromise()
      .then(response => response.blob());
  }

}
