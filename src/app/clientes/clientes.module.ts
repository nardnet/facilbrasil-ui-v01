import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesCadastroComponent } from './clientes-cadastro/clientes-cadastro.component';
import { ClientesPesquisaComponent } from './clientes-pesquisa/clientes-pesquisa.component';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ButtonModule } from 'primeng/components/button/button';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { NgxMaskModule } from 'ngx-mask';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ClientesContratoComponent } from './clientes-contrato/clientes-contrato.component';
import { ClientesSucessoComponent } from './clientes-sucesso/clientes-sucesso.component';

import { LoadingModule } from 'ngx-loading';
import { ClientesStatusComponent } from './clientes-status/clientes-status.component';
import { ClientesEmailsComponent } from './clientes-emails/clientes-emails.component';
import { ClientesVideoComponent } from './clientes-video/clientes-video.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    NgxMaskModule.forRoot(),
    SharedModule,
    RouterModule,
    LoadingModule.forRoot({
      fullScreenBackdrop: true,
      backdropBorderRadius: '14px'
  })
  ],
  declarations: [
    ClientesCadastroComponent,
    ClientesPesquisaComponent,
    ClientesContratoComponent,
    ClientesSucessoComponent,
    ClientesStatusComponent,
    ClientesEmailsComponent,
    ClientesVideoComponent
  ],
  exports: [
    ClientesCadastroComponent,
    ClientesPesquisaComponent,
    ClientesSucessoComponent
  ]
})
export class ClientesModule { }
