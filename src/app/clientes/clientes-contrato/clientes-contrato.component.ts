import { Component, OnInit, Input } from '@angular/core';
import { Clientes } from '../../core/model';

@Component({
  selector: 'app-clientes-contrato',
  templateUrl: './clientes-contrato.component.html',
  styleUrls: ['./clientes-contrato.component.css']
})
export class ClientesContratoComponent implements OnInit {
  @Input() clientes = new Clientes();
  constructor() { }

  ngOnInit() {
  }

}
