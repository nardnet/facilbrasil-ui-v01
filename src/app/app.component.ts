import { Component, OnInit } from '@angular/core';
import { ToastyConfig } from 'ng2-toasty';
import { Router } from '@angular/router';
import { AuthService } from './seguranca/auth.service';
import { LogoutService } from './seguranca/logout.service';
import { ErrorHandlerService } from './core/error-handler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  usuario: string;
  email: string;
  codigo: string;
  visivel: boolean;

  constructor(
    private toastyConfig: ToastyConfig,
    private router: Router,
    public auth: AuthService,
    private logoutService: LogoutService,
    private errorHandler: ErrorHandlerService
  ) {
    this.toastyConfig.theme = 'bootstrap';
  }

  hasSomething() {
    return window.location.href.indexOf('something') > -1;
  }

  open(e) {
    if (this.auth.jwtPayload.codigoCliente) {
      this.router.navigate(['/titulos/titulos-principal']);
    } else {
      this.router.navigate(['/clientes']);
    }
    e.stopPropagation();
  }

  exibindoNavbar() {
    return ((this.router.url !== '/login') && (this.router.url !== '/cadastro'));
  }

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
